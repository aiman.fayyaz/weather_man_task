import csv


months_dict = {
    1: "Jan",
    2: "Feb",
    3: "Mar",
    4: "Apr",
    5: "May",
    6: "Jun",
    7: "Jul",
    8: "Aug",
    9: "Sep",
    10: "Oct",
    11: "Nov",
    12: "Dec"
}


class WeatherDataParser:

    def parse_monthly_weather_file(month_no, year):
        monthly_weather_data = []   # for holding daily weather data
        weather_attributes = [
            "PKT","Max TemperatureC", "Mean TemperatureC","Min TemperatureC",
            " Mean Humidity", "Max Humidity", " Min Humidity"
            ]
        try:
            month_name = months_dict[month_no]
            file_name = f"weatherfiles/Murree_weather_{year}_{month_name}.txt"
            with open(file_name) as weather_file:
                for row in csv.DictReader(weather_file):
                    daily_weather_data = {}
                    for attribute in weather_attributes:
                        if(attribute == "PKT"):
                            daily_weather_data[attribute] = row[attribute]
                        else:
                            daily_weather_data[attribute] = int(row[attribute] or 0)
                    monthly_weather_data.append(daily_weather_data)
        except FileNotFoundError:
            pass
        finally:
            return monthly_weather_data

    @staticmethod    
    def parse_yearly_weather_files(year):
        yearly_weather_data = {}
        for month_no in range(1,13):
            month_name = months_dict[month_no]
            yearly_weather_data[month_name] = WeatherDataParser.parse_monthly_weather_file(month_no, year)
        return yearly_weather_data
