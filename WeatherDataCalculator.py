class WeatherDataCalculator:

    @staticmethod
    def compute_avg_highest_temp(weather_data):
        max_temp_list = []
        for daily_weather in weather_data:
            if(daily_weather["Max TemperatureC"] != 0):
                max_temp_list.append(daily_weather["Max TemperatureC"])
        return round((sum(max_temp_list)/len(max_temp_list)))
    
    @staticmethod
    def compute_avg_lowest_temp(weather_data):
        min_temp_list = []
        for daily_weather in weather_data:
            if(daily_weather["Min TemperatureC"] != 0):
                min_temp_list.append(daily_weather["Min TemperatureC"])
        return round((sum(min_temp_list)/len(min_temp_list)))
    
    @staticmethod
    def compute_avg_mean_humidity(weather_data):
        mean_humidity_list = []
        for daily_weather in weather_data:
            if(daily_weather[" Mean Humidity"] != 0):
                mean_humidity_list.append(daily_weather[" Mean Humidity"])
        return round((sum(mean_humidity_list)/len(mean_humidity_list)))
    

    @staticmethod
    def compute_highest_temp_and_day(weather_data):
        max_temp_day = ""
        max_temp = 0 
        for month, month_data in weather_data.items():
            for daily_data in month_data:
                if(daily_data["Max TemperatureC"] > max_temp):
                    max_temp = daily_data["Max TemperatureC"]
                    max_temp_day = daily_data["PKT"]
        return {"max_temp": str(max_temp) + "C", "max_temp_day": max_temp_day}
    
    @staticmethod
    def compute_max_humidity_and_day(weather_data):
        max_humidity_day = ""
        max_humidity = 0 
        for month, month_data in weather_data.items():
            for daily_data in month_data:
                if(daily_data["Max Humidity"] > max_humidity):
                    max_humidity = daily_data["Max Humidity"]
                    max_humidity_day = daily_data["PKT"]
        return {"max_humidity": str(max_humidity) + "%", "max_humidity_day": max_humidity_day}
    
    @staticmethod
    def compute_lowest_temp_and_day(weather_data):
        min_temp_day = ""
        min_temp = 0 
        for month, month_data in weather_data.items():
            for daily_data in month_data:
                if(daily_data["Min TemperatureC"] > min_temp):
                    min_temp = daily_data["Min TemperatureC"]
                    min_temp_day = daily_data["PKT"]
                    min_temp_month = month
        return {"min_temp": str(min_temp) + "C", "min_temp_day": min_temp_day}
    




