from WeatherDataParser import WeatherDataParser
from WeatherReportGenerator import WeatherReportGenerator


print(f"{'-'*10}Murree Weather Data from 2004-2016{'-'*10}")

start_year = 2004
end_year = 2016


def ComputeWeatherData():
    data = input("Enter 'M' for monthly and 'Y' for yearly Weather data: ")

    if data.upper() == "M":
        month_no = int(input("Enter month number: "))
        year = input("Enter year: ")
        weather_data = WeatherDataParser.\
            parse_monthly_weather_file(month_no, year)
        if(len(weather_data) == 0):
            print("Sorry. Weather data for this month does not exist.")
            return 
        WeatherReportGenerator.generate_monthly_weather_report(weather_data)
        WeatherReportGenerator.plot_two_bar_charts(weather_data)
        WeatherReportGenerator.plot_single_bar_chart(weather_data)


    elif data.upper() == "Y":
        year = int(input("Enter year: "))
        if(start_year <= year <= end_year):
            weather_data = WeatherDataParser.\
                parse_yearly_weather_files(year)
            WeatherReportGenerator.\
                generate_yearly_weather_report(weather_data)
    
    else:
        print("Please enter either M or Y!")


try:
    total_reports = \
    int(input("How many weather reports do you want to generate: "))
    if(total_reports < 0):
        raise Exception("Only positive integers allowed.")
    for i in range(total_reports):
        ComputeWeatherData()
except ValueError:
    print("Please enter an integer!")
