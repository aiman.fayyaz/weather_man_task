from WeatherDataCalculator import WeatherDataCalculator
from datetime import datetime


months_dict = {
    1: "Jan",
    2: "Feb",
    3: "Mar",
    4: "Apr",
    5: "May",
    6: "Jun",
    7: "Jul",
    8: "Aug",
    9: "Sep",
    10: "Oct",
    11: "Nov",
    12: "Dec"
}

class WeatherReportGenerator:

    def generate_monthly_weather_report(weather_data):
        date = (datetime.strptime(weather_data[0]["PKT"], "%Y-%m-%d")).date()
        month_name = months_dict[date.month]
        year = date.year
        print(f"\nWeather Report for {month_name} {year}:")
        monthly_report = {}
        avg_max_temp = WeatherDataCalculator.compute_avg_highest_temp(weather_data)
        monthly_report["Average Highest Temperature"] = str(avg_max_temp) + "C"

        avg_min_temp = WeatherDataCalculator.compute_avg_lowest_temp(weather_data)
        monthly_report["Average Lowest Temperature"] = str(avg_min_temp) + "C"

        avg_mean_humidity = WeatherDataCalculator.compute_avg_mean_humidity(weather_data)
        monthly_report["Average Mean Humidity"] = str(avg_mean_humidity) + "%"
        for key, value in monthly_report.items():
            print(f"{key}: {value}")

    def plot_two_bar_charts(weather_data):
        for daily_data in weather_data:
            date = daily_data["PKT"]
            print(datetime.strptime(date, "%Y-%m-%d").date().day, end = " ")
            max_temp = daily_data["Max TemperatureC"]
            print(f"\033[0;31m{'+'*max_temp}\033[0m {max_temp}C")
            min_temp = daily_data["Min TemperatureC"]
            print(datetime.strptime(date, "%Y-%m-%d").date().day, end = " ")
            print(f"\033[0;34m{'+'*min_temp}\033[0m {min_temp}C")

    def plot_single_bar_chart(weather_data):
        for daily_data in weather_data:
            date = daily_data["PKT"]
            print(datetime.strptime(date, "%Y-%m-%d").date().day, end = " ")
            min_temp = daily_data["Min TemperatureC"]
            print(f"\033[0;34m{'+'*min_temp}\033[0m", end = "")
            max_temp = daily_data["Max TemperatureC"]
            print(f"\033[0;31m{'+'*max_temp}\033[0m",end = " ")
            print(f"{min_temp}C-{max_temp}C")



    def generate_yearly_weather_report(weather_data):
        yearly_report = {}
        max_temp = WeatherDataCalculator.compute_highest_temp_and_day(weather_data)
        yearly_report["Highest"] = max_temp
        min_temp = WeatherDataCalculator.compute_lowest_temp_and_day(weather_data)
        yearly_report["Lowest"] = min_temp
        max_humidity = WeatherDataCalculator.compute_max_humidity_and_day(weather_data)
        yearly_report["Humidity"] = max_humidity
        for key, value_dict in yearly_report.items():
            value, day = value_dict.values()
            month_no = datetime.strptime(day, "%Y-%m-%d").date().month
            date = datetime.strptime(day, "%Y-%m-%d").date().day
            month_name = months_dict[month_no]
            print(f"{key}: {value} on {month_name} {date}")

